exports.success = function (req, res, msg = '', status = 200) {

    res.status(status).send({
        error: false,
        status: status,
        body: msg
    });
};

exports.error = function (req, res, msg, status = 500) {
    const messageerror = msg || 'Error interno del servidor';
    res.status(status).send({
        error: true,
        status: status,
        body: msg
    });
};