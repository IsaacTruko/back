const response = require('../../connect/response');

const auth = require('../auth');

const TABLE = 'Tarea';


module.exports = function(dbInyector){

    let db = dbInyector;

    if(!db){
        db = require('../../DB/mysql');
    }

    function all(){
        return db.all(TABLE)
    }
    
    function one(id){
        return db.one(TABLE, id)
    }

    function add(body){
        return db.add(TABLE, body)
    }
    
    
    function eraseone(body){
        return db.eraseone(TABLE, body)
    }

    function disable(body){
        return db.disable(TABLE, body)
    }

    function selectTarea(id){

        return db.selectTarea(TABLE, id)
    }

    function selectAllTareas(){

        return db.selectAllTarea(TABLE) 
    }
    
    return {
        all,
        one,
        add,
        eraseone,
        disable,
        selectTarea,
        selectAllTareas
    }
}