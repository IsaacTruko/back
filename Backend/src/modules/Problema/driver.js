const response = require('../../connect/response');

const auth = require('../auth');

const TABLE = 'Problema';


module.exports = function(dbInyector){

    let db = dbInyector;

    if(!db){
        db = require('../../DB/mysql');
    }

    function all(){
        return db.all(TABLE)
    }
    
    function one(id){
        return db.one(TABLE, id)
    }

    function add(body){
        return db.add(TABLE, body)
    }
    
    
    function eraseone(body){
        return db.eraseone(TABLE, body)
    }

    function disable(body){
        return db.disable(TABLE, body)
    }

    function selectProblema(id){

        return db.selectProblema(TABLE, id)
    }

    function selectAllProblema(){

        return db.selectAllProblema(TABLE)
    }
    
    return {
        all,
        one,
        add,
        eraseone,
        disable,
        selectProblema,
        selectAllProblema
    }
}