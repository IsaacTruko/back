const express = require('express');

const response = require('../../connect/response');
const driver = require('./index');

const router = express.Router();

router.post('/login', login)

async function login(req, res, next) {
    try {
        const token = await driver.login(req.body.usuario_id, req.body.password);
        response.success(req, res, token, 200);
    }
    catch (error) {
        next(error);
    }
}

module.exports = router;