const db = require('../../DB/mysql');
const bcrypt = require('bcrypt');
const auth = require('../../auth');

const response = require('../../connect/response');

const TABLE = 'Authentication';

module.exports = function(dbInyector) {

    if(!dbInyector){
    let db = dbInyector || require('../../DB/mysql');
    }

    async function login(id, password) {
        const data = await db.query(TABLE, {usuario_id: id});

        return bcrypt.compare(password, data.password)
            .then(result => {
                if(result) {
                    return auth.giveToken({id: id});
                } else {
                    throw new Error('Contraseña incorrecta');
                }
            })
            .catch(error => {
                throw error;
            });
    }


    async function add(data) {
        const authData = {
            usuario_id: data.usuario_id,
        };

        authData.password = await bcrypt.hash(data.password.toString(), 10);
       

        return db.add(TABLE, authData);
    }
    
    return {
        add,
        login,
    };
}
