function error (message, code){
    let e = new Error(message);
    
    if(code){
        e.code = code;
    }
    return e;
}

module.exports = error;
